# make sure this is at the top if it isn't already
from django import forms
from .models import Item, Category, Supplier


# our new form
class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('title', 'description', 'barcode', 'stock_count', 'isActive', 'category','supplier')
        labels = {
            'isActive': 'Enable Product',
        }
        widgets = {
            "title": forms.TextInput(attrs={'placeholder': 'Product Name', 'id': 'title', 'class': 'md-input'}),
            "description": forms.TextInput(
                attrs={'placeholder': 'Description', 'id': 'Description', 'class': 'md-input'}),
            "stock_count": forms.NumberInput(
                attrs={'placeholder': 'Enter Stock', 'class': 'md-input'}),
            "barcode": forms.TextInput(attrs={'placeholder': 'UPC/EAN/ISBN', 'id': 'barcode', 'class': 'md-input'}),
            "isActive": forms.CheckboxInput(
                attrs={'placeholder': 'Enable', 'id': 'activeswitch', 'class': 'md-switch'}),
        }

    class Media:
        js = ('product/js/product.js',)

    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        self.fields['barcode'].required = False


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('cat_name', 'cat_description')
        labels = {
            'cat_name': 'Category Name',
            'cat_description': 'Category Description'
        }
        widgets = {
            'cat_name': forms.TextInput(attrs={'placeholder': 'Category', 'id': 'cate_name', 'class': 'md-input'}),
            'cat_description': forms.TextInput(
                attrs={'placeholder': 'Description', 'id': 'Description', 'class': 'md-input'}),
        }


class SupplierForm(forms.ModelForm):
    class Meta:
        model = Supplier
        agency_name = forms.CharField(required=False)
        email = forms.EmailField(widget=forms.EmailInput())

        # TODO implement phone number field
        fields = ('company_name', 'agency_name', 'full_name', 'email', 'phone_number','address1','address2', 'city',
                  'state', 'zip','country','comments')
        labels = {
            'address1': 'Address 1',
            'address2': 'Address 2',
        }
        widgets = {
            "company_name": forms.TextInput(attrs={'placeholder': 'Company Name', 'class': 'md-input'}),
            "agency_name": forms.TextInput(attrs={'placeholder': 'Agency Name', 'class': 'md-input'}),
            "full_name": forms.TextInput(attrs={'placeholder': 'Full Name', 'class': 'md-input'}),
            "email": forms.EmailInput(attrs={'placeholder': 'Email', 'class': 'md-input'}),
            "phone_number": forms.NumberInput(attrs={'placeholder': 'Phone Number', 'class': 'md-input'}),
            "address1": forms.TextInput(attrs={'placeholder': 'Address 1', 'class': 'md-input'}),
            "address2": forms.TextInput(attrs={'placeholder': 'Address 2', 'class': 'md-input'}),
            "city": forms.TextInput(attrs={'placeholder': 'Enter City Name', 'class': 'md-input'}),
            "state": forms.TextInput(attrs={'placeholder': 'Enter State Name', 'class': 'md-input'}),
            "zip": forms.TextInput(attrs={'placeholder': 'Enter Zip Code', 'class': 'md-input'}),
            "country": forms.TextInput(attrs={'placeholder': 'Enter Country', 'class': 'md-input'}),
            "comments": forms.TextInput(attrs={'placeholder': 'Enter Comments', 'class': 'md-input'}),
        }

        def __init__(self, *args, **kwargs):
            super(SupplierForm, self).__init__(*args, **kwargs)
            self.fields['agency_name'].required = False
            self.fields['address2'].required = False
            self.fields['comments'].required = False
